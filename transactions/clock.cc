#include "clock.h"

Timestamp Clock::current() const { return next_; }

Timestamp Clock::next() { return ++next_; }

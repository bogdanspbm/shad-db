#pragma once

#include "row.h"
#include "schema.h"

#include <memory>

namespace shdb {

struct Rowset
{
    std::shared_ptr<Schema> schema;
    std::vector<Row *> rows;

    Rowset();
    Rowset(std::shared_ptr<Schema> schema);
    Rowset(const Rowset &) = delete;
    Rowset(Rowset &&other);
    ~Rowset();

    Row *allocate();
    void sort_rows(int (*comparer)(const Row *lhs, const Row *rhs));
};

struct Hashset
{
    std::shared_ptr<Schema> schema;
    std::unordered_map<Row, Rowset> key_to_rowset;

    Hashset();
    Hashset(std::shared_ptr<Schema> schema);
    Hashset(const Hashset &) = delete;
    Hashset(Hashset &&other);

    Rowset *find(Row *key);
    Rowset *find_or_create(Row *key);
};

struct Groupset
{
    Rowset keys;
    size_t state_size;
    std::vector<char *> states;
    std::unordered_map<Row, size_t> key_to_index;

    Groupset();
    Groupset(std::shared_ptr<Schema> key_schema, size_t state_size);
    Groupset(const Groupset &) = delete;
    Groupset(Groupset &&other);
    ~Groupset();

    void *find_state(Row *key);
    void *create_state(Row *key);
};

}    // namespace shdb

#include <cassert>

#include "storage.h"

Value Storage::get_at(Timestamp at, Key key) const {
  // TODO: implement this.
  return Value{};
}

const std::pair<const Timestamp, Storage::TagValue> *
Storage::get_last(Key key) {
  // TODO: implement this.
  return nullptr;
}

void Storage::apply(Timestamp ts, TransactionId txid, Key key, Value value) {
  // TODO: implement this.
}

void Storage::rollback(Timestamp ts, TransactionId txid, Key key, Value value) {
  // TODO: implement this.
}

void Storage::dump(
    std::vector<std::tuple<Key, Timestamp, TagValue>> *out) const {
  // TODO: implement this.
}

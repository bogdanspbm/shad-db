#!/bin/bash

set -e
set -x

# 2022
usernames=(
  "gepardo"
  "bkuchin"
  "erzyadev"
  "kononovk"
  "zeronsix"
  "nsvasilev"
  "faucct"
  "svlads"
  "d4rk"
  "kitaisreal"
  "Fedya001"
  "AlexJokel"
  "yesenarman"
  "gripon"
  "Kifye"
  "dkhorkin"
  "mbkkt"
  "pervakovg"
  "Panesher"
  "TEduard"
  "alexeysm"
  "bazarinm"

  "dnorlov"
)

ulimit -n 64000
make -C shad-db/threads tester

port=1300

for run in `seq 1 4`
do
  for testid in `seq 1 6`
  do
    for username in ${usernames[@]}
    do
      skip="logs/${username}.${run}.${testid}.skip"
      stderr="logs/${username}.${run}.${testid}.stderr"
      stdout="logs/${username}.${run}.${testid}.stdout"
      fail="logs/${username}.${run}.${testid}.fail"
      failptrn="logs/${username}.?.${testid}.fail"
      
      if ./blacklist.py ${username} ${testid} || ls $failptrn;
      then
        touch $skip
      else
        if ls $stdout
        then
          echo "Run ${run}, test ${testid} for ${username} already done"
        else
          let port=port+1
          binary="bins/${username}"
          spec="shad-db/threads/performance/test.${testid}.json"
          timeout 600 shad-db/threads/tester ${binary} ${port} $spec 2> $stderr > $stdout || touch $fail
          sleep 5
        fi
      fi
    done
  done
done

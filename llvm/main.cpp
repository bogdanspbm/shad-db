#include "jit.h"
#include "flow.h"
#include "answers.h"

#include <iostream>
#include <cassert>

using namespace shdb;

void test_dummy()
{
    auto jit = Jit();
    auto callback = answer_dummy(jit);
    callback();
    std::cout << "test_dummy PASSED" << std::endl;
}

void test_constant()
{
    for (int i = -1; i < 2; ++i) {
        auto jit = Jit();
        auto callback = answer_constant(jit, i);
        assert(callback() == i);
    }
    std::cout << "test_constant PASSED" << std::endl;
}

void test_trivial()
{
    auto jit = Jit();
    auto callback = answer_trivial(jit);
    for (int i = -10; i < 20; ++i) {
        assert(callback(i) == i);
    }
    std::cout << "test_trivial PASSED" << std::endl;
}

void test_sum()
{
    {
        auto jit = Jit();
        auto callback = reinterpret_cast<int(*)(int)>(answer_sum(jit, 1));
        for (int i = -10; i < 20; ++i) {
            assert(callback(i) == i);
        }
    }
    {
        auto jit = Jit();
        auto callback = reinterpret_cast<int(*)(int, int)>(answer_sum(jit, 2));
        for (int i = -10; i < 20; ++i) {
            assert(callback(i, i+1) == i + i+1);
        }
    }
    {
        auto jit = Jit();
        auto callback = reinterpret_cast<int(*)(int, int, int)>(answer_sum(jit, 3));
        for (int i = -10; i < 20; ++i) {
            assert(callback(i, i+1, i+2) == i + i+1 + i+2);
        }
    }
    std::cout << "test_sum PASSED" << std::endl;
}

static int call_store;

int helper_call()
{
    return call_store;
}

void test_call()
{
    auto jit = Jit();
    auto callback = answer_call(jit);
    call_store = 3;
    assert(callback() == 3);
    call_store = 30;
    assert(callback() == 30);
    std::cout << "test_call PASSED" << std::endl;
}

void test_abs()
{
    auto jit = Jit();
    auto callback = answer_abs(jit);
    assert(callback(10) == 10);
    assert(callback(-10) == 10);
    std::cout << "test_abs PASSED" << std::endl;
}

static int for_store;
int helper_for()
{
    return for_store++;
}

void test_for()
{
    auto jit = Jit();
    auto callback = answer_for(jit);
    assert(callback(10) == 5 * 9);
    assert(for_store == 10);
    std::cout << "test_for PASSED" << std::endl;
}

int main()
{
    test_dummy();
    test_constant();
    test_trivial();
    test_sum();
    test_call();
    test_abs();
    test_for();
    return 0;
}

